'use strict';

/* http://docs.angularjs.org/guide/dev_guide.e2e-testing */

describe('Mobile SIA Automated Testing', function() {

  // Testing Halaman Splash
  describe('Halaman Splash', function() {
    beforeEach(function() {
      browser().navigateTo('../../app/index.html');
    });

    it('URL pada posisi / (Halaman Splash)', function() {
      expect(browser().location().url()).toBe("/");
      expect(element('h1').text()).toMatch('Mobile SIA');
      browser().navigateTo('#/login');
    });

  });


  // Testing Halaman Login
  describe('Halaman Login', function() {

    it('Url berpindah ke /login, Aplikasi menampilkan form "login"', function() {
      expect(browser().location().url()).toBe("/login");
      expect(element('h2').text()).toMatch('Login');
    });

    it('input NIM dan password (09650048, 123456)', function() {
      input('username').enter('09650048');
      input('password').enter('123456');
      expect(element('input[name=username]').val()).toEqual('09650048');
      expect(element('input[name=password]').val()).toEqual('123456');
    });

    it('Klik tombol "Login"', function() {
      element('#btlogin').click();
    });

    it('Url berpindah ke /home', function() {
      expect(browser().location().url()).toBe("/home");
    });

  });


  // Testing Halaman Home
  describe('Halaman Home', function() {

    it('Akan menampilkan halaman home dan pengumuman pada element div dengan id=umum', function() {
      expect(browser().location().url()).toBe("/home");
      expect(element('#umum').text()).toMatch('Perubahan masa KRS Semester Gasal 2013');
      browser().navigateTo('#/semester');
    });
  });


   // Testing Halaman KHS Semester
  describe('Halaman KHS Semester', function() {

    it('Url berpindah ke /semester, Aplikasi menampilkan form "semester"', function() {
      expect(browser().location().url()).toBe("/semester");
      expect(element('.title').text()).toMatch('KHS Semester');
    });

    it('Saya memilih opsi tahun "2012" dari combobox "ta" dan opsi semester "1 / Ganjil" dari combobox "semester" ', function() {
      select('parameter.tahun').option('2011');
      select('parameter.semester').option('0');
      expect(element('select[name=ta] option:selected').text()).toEqual('2011/2012');
      expect(element('select[name=semester] option:selected').text()).toEqual('Semester Ganjil');
    });

    it('Klik tombol "Cek KHS"', function() {
      element('#bt_semester').click(); 
    });

    it('Aplikasi menampilkan list KHS Semester', function() {
       expect(repeater('table tbody tr').count()).toBeGreaterThan(0);
       browser().navigateTo('#/kumulatif');
    });

  });


  // Testing Halaman KHS Kumulatif
  describe('Halaman KHS Kumulatif', function() {

    it('Url berpindah ke /kumulatif, Aplikasi menampilkan form "kumulatif"', function() {
      expect(browser().location().url()).toBe("/kumulatif");
      expect(element('.title').text()).toMatch('KHS Kumulatif');
    });

    it('Aplikasi menampilkan list KHS Kumulatuf', function() {
       expect(repeater('table#tbl_kumulatif tbody tr').count()).toBeGreaterThan(0);
       browser().navigateTo('#/sejarah');
    });

  });


  // Testing Halaman Sejarah Indeks Prestasi
  describe('Halaman Sejerah Indeks Prestasi', function() {

    it('Url berpindah ke /sejarah, Aplikasi menampilkan form "sejarah"', function() {
      expect(browser().location().url()).toBe("/sejarah");
      expect(element('.title').text()).toMatch('Sejarah IP');
    });

    it('Aplikasi menampilkan list Sejarah Indeks Prestasi', function() {
       expect(repeater('table#tbl_sejarahip tbody tr').count()).toBeGreaterThan(0);
       browser().navigateTo('#/kuliah');
    });

  });


  // Testing Halaman Jadwal Kuliah
  describe('Halaman Jadwal Kuliah', function() {

    it('Url berpindah ke /kuliah, Aplikasi menampilkan form "kuliah"', function() {
      expect(browser().location().url()).toBe("/kuliah");
      expect(element('.title').text()).toMatch('Jadwal Kuliah');
    });

    it('Saya memilih opsi tahun "2012" dari combobox "ta" dan opsi semester "1 / Ganjil" dari combobox "semester" ', function() {
      select('parameter.tahun').option('2011');
      select('parameter.semester').option('0');
      expect(element('select[name=ta] option:selected').text()).toEqual('2011/2012');
      expect(element('select[name=semester] option:selected').text()).toEqual('Semester Ganjil');
    });

    it('Klik tombol "Cek Jadwal"', function() {
      element('#bt_kuliah').click(); 
    });

    it('Aplikasi menampilkan list Jadwal Kuliah', function() {
       expect(repeater('table#tbl_jadwal_kuliah tbody tr').count()).toBeGreaterThan(0);
       browser().navigateTo('#/ujian');
    });

  });


  // Testing Halaman Jadwal Ujian
  describe('Halaman Jadwal Ujian', function() {

    it('Url berpindah ke /ujian, Aplikasi menampilkan form "ujian"', function() {
      expect(browser().location().url()).toBe("/ujian");
      expect(element('.title').text()).toMatch('Jadwal Ujian');
    });

    it('Saya memilih opsi tahun "2012" dari combobox "ta" dan opsi semester "1 / Ganjil" dari combobox "semester" ', function() {
      select('parameter.tahun').option('2011');
      select('parameter.semester').option('0');
      expect(element('select[name=ta] option:selected').text()).toEqual('2011/2012');
      expect(element('select[name=semester] option:selected').text()).toEqual('Semester Ganjil');
    });

    it('Klik tombol "Cek Jadwal"', function() {
      element('#bt_ujian').click(); 
    });

    it('Aplikasi menampilkan list Jadwal Ujian', function() {
       expect(repeater('table#tbl_jadwal_ujian tbody tr').count()).toBeGreaterThan(0);
       browser().navigateTo('#/presensi');
    });

  });


   // Testing Halaman Presensi
  describe('Halaman Presensi', function() {

    it('Url berpindah ke /presensi, Aplikasi menampilkan form "presensi"', function() {
      expect(browser().location().url()).toBe("/presensi");
      expect(element('.title').text()).toMatch('Presensi');
    });

    it('Saya memilih opsi tahun "2012" dari combobox "ta" dan opsi semester "1 / Ganjil" dari combobox "semester" ', function() {
      select('parameter.tahun').option('2011');
      select('parameter.semester').option('0');
      expect(element('select[name=ta] option:selected').text()).toEqual('2011/2012');
      expect(element('select[name=semester] option:selected').text()).toEqual('Semester Ganjil');
    });

    it('Klik tombol "Cek Presensi"', function() {
      element('#bt_presensi').click(); 
    });

    it('Aplikasi menampilkan list Presensi', function() {
       expect(repeater('table#tbl_presensi tbody tr').count()).toBeGreaterThan(0);
       browser().navigateTo('#/tentang');
    });

  });

   // Testing Halaman Tentang Aplikasi
  describe('Halaman Tentang Aplikasi', function() {

    it('Url berpindah ke /tentang, Aplikasi menampilkan form "tentang aplikasi"', function() {
      expect(browser().location().url()).toBe("/tentang");
      expect(element('.title').text()).toMatch('Tentang Aplikasi');
      browser().navigateTo('#/umpan');
    });
  });

   // Testing Halaman Umpan Balik
  describe('Halaman Umpan Balik', function() {

    it('Url berpindah ke /umpan, Aplikasi menampilkan form "umpan"', function() {
      expect(browser().location().url()).toBe("/umpan");
      expect(element('.title').text()).toMatch('Umpan Balik');
    });

    it('Saya mengisikan kritik dan saran', function() {
      input('isi').enter("Testing alpha");
    });

    it('Saya klik tombol "kirim"', function() {
      element('#bt_umpan').click(); 
    });

  });



});
